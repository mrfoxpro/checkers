﻿using Checkers.Server.Service;
using Checkers.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
namespace Checkers.Server.Controllers
{
    [Route("api/[controller]")]
    public class LobbyController : Controller
    {
        private readonly IJwtTokenService _tokenService;
        public LobbyController(IJwtTokenService tokenService)
        {
            _tokenService = tokenService;
        }

        [HttpPost("[action]")]
        public IActionResult Login([FromBody] TokenViewModel login)
        {
            var token = _tokenService.BuildToken(login.NickName);
            return Ok(new { token });
        }

        [Authorize(AuthenticationSchemes = "Bearer")]
        [HttpGet("[action]")]
        public IActionResult CreateAccount()
        {
            Player player = new Player
            {
                ID = User.FindFirst(JwtRegisteredClaimNames.Jti).Value,
                NickName = User.FindFirst("nick").Value
            };
            Game.ConnectedPlayers.Add(player);
            return Redirect("/lobbies");
        }

        [Authorize(AuthenticationSchemes = "Bearer")]
        [HttpGet("[action]")]
        public IActionResult Exit()
        {
            var pl = Game.ConnectedPlayers.Find(x => x.ID == User.FindFirst(JwtRegisteredClaimNames.Jti).Value);
            Game.ConnectedPlayers.Remove(pl);
            return Redirect("/");
        }

        [HttpGet("[action]")]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public List<Lobby> Lobbies()
        {
            Console.WriteLine(User.Identity.IsAuthenticated);
            return Game.Lobbies;
        }

        [HttpPost("[action]")]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public IActionResult CreateLobby(string name)
        {
            var l = new Lobby
            {
                Name = name,
                Player1 = Game.ConnectedPlayers.Find(x => x.ID == User.FindFirst(JwtRegisteredClaimNames.Jti).Value)
            };
            Game.Lobbies.Add(l);
            return Ok();
        }
    }
    public class TokenViewModel
    {
        public string NickName { get; set; }
    }
}
