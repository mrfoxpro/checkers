﻿using Checkers.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Checkers.Server
{
    public class Game
    {
        public static List<Lobby> Lobbies = new List<Lobby> {
            new Lobby{
                Name = "Test",
                Player1 = new Player{
                    NickName="TestPlayer1"
                },
                Player2 = new Player {
                    NickName ="TestPlayer2"
                }
            }
        };
        public static List<Player> ConnectedPlayers = new List<Player>();
    }
}
