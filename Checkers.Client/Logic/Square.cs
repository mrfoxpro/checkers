﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Checkers.Client.Logic
{
    public class Square
    {
        public int ID { get; set; }
        public bool ocupied { get; set; }

        public int pieceId { get; set; }
    }
}
