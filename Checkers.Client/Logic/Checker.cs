﻿using Checkers.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Checkers.Client.Logic
{
    public class Checker
    {
        public string ID { get; set; }
        public CheckersColor Color { get; set; }
        public bool IsQueen { get; set; } = false;
        public object ocupied_square { get; set; }
        public bool IsAlive { get; set; } = true;

        public bool IsAttack { get; set; } = false;
        public int Square { get; set; }

        public int coordX { get; set; }
        public int coordY { get; set; }
        public void setCoord(int X, int Y)
        {
            int x = (coordX - 1) * Game.moveLength + Game.moveDeviation;
            int y = (coordY - 1) * Game.moveLength + Game.moveDeviation;
        }
    }
}
