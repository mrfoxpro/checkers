﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Checkers.Client.Logic
{
    public class Register : ComponentBase
    {
        [Inject]
        IJSRuntime JSRuntime { get; set; }
        [Inject]
        IUriHelper UriHelper { get; set; }
        [Inject]
        HttpClient Http { get; set; }
        protected string NickName { get; set; }
        protected async void OnEnterButtonClicked()
        {
            if (string.IsNullOrEmpty(NickName))
            {
                await JSRuntime.InvokeAsync<object>("alert", "Логин не может быть пустым!");
                return;
            }
            var responseLogin = await Http.PostJsonAsync<TokenResponse>("api/Lobby/Login", new
            {
                NickName
            });
            Http.DefaultRequestHeaders.Remove("Authorization");
            Http.DefaultRequestHeaders.Add("Authorization", string.Format("Bearer {0} ", responseLogin.Token));
            await Http.GetAsync("api/Lobby/CreateAccount");
            UriHelper.NavigateTo("/lobbies");
        }
    }

    internal class TokenResponse
    {
        public string Token { get; set; }
    }
}
