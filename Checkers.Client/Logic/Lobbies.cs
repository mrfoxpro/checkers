﻿using Blazor.Extensions;
using Checkers.Shared;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Checkers.Client.Logic
{
    public class Lobbies : ComponentBase
    {
        [Inject]
        IJSRuntime JSRuntime { get; set; }
        [Inject]
        HttpClient Http { get; set; }
        [Inject]
        IUriHelper UriHelper { get; set; }
        protected List<Lobby> lobbies = new List<Lobby>();
        protected string NewRoomName { get; set; }
        protected async override void OnInit()
        {
            await Task.Run(async () =>
            {
                while (true)
                {
                    bool shouldRerender = false;
                    var ls = await Http.GetJsonAsync<List<Lobby>>("api/lobby/lobbies");
                    if (ls.Count != lobbies.Count) shouldRerender = true;
                    lobbies = ls;
                    if (shouldRerender)
                        StateHasChanged();
                    await Task.Delay(2000);
                }
            });
            base.OnInit();
        }
        protected async void OnNewRoomButtonClicked()
        {
            if (string.IsNullOrEmpty(NewRoomName))
            {
                await JSRuntime.InvokeAsync<object>("alert", "Название комнаты не может быть пустым!");
                return;
            }
            var s = new
            {
                name = NewRoomName
            };
            await Http.PostJsonAsync("api/lobby/CreateLobby", s);
        }
        protected async void OnExitButonClicked()
        {
            await Http.GetAsync("api/lobby/exit");
            // idk why not redirect
            UriHelper.NavigateTo("/");
        }
    }
}
