﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.RenderTree;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Checkers.Client.Logic
{
    public class Game : ComponentBase
    {
        [Inject]
        IJSRuntime JSRuntime { get; set; }
        [Inject]
        HttpClient Http { get; set; }
        [Inject]
        IUriHelper UriHelper { get; set; }

        public static RenderTreeBuilder Builder;

        public static int moveLength = 80;
        public static int moveDeviation = 10;
        protected override void OnInit()
        {
            CreateCheckers();
            base.OnInit();
        }

        private void CreateCheckers()
        {

        }

        protected override void BuildRenderTree(RenderTreeBuilder builder)
        {
            base.BuildRenderTree(builder);
            Console.WriteLine("rendering");
            builder.OpenElement(0, "table");
            builder.OpenElement(1, "tbody");

            for (var row = 0; row < 3; row++)
            {
                builder.OpenElement(2, "tr");
                for (var col = 0; col < 3; col++)
                {
                    builder.OpenElement(3, "td");
                    builder.AddAttribute(4, "class", "tictactoe-cell");
                    builder.CloseElement();
                }

                builder.CloseElement();
            }

            builder.CloseElement();
            builder.CloseElement();
            
            //int ct = 0;
            //builder.OpenElement(ct++, "svg");
            //builder.AddAttribute(ct++, "height", 800);
            //builder.AddAttribute(ct++, "width", 800);
            //builder.AddAttribute(ct++, "style", "text-align:center;");
            //DrawBoard(builder, ref ct);
            //builder.CloseElement();
            //base.BuildRenderTree(builder);

        }

        private void DrawBoard(RenderTreeBuilder builder, ref int ct)
        {
            ct = DrawSquares(builder, ct);
            ct = DrawCheckers(builder, ct);
        }

        private int DrawCheckers(RenderTreeBuilder builder, int ct)
        {
            return ct;
        }

        private int DrawSquares(RenderTreeBuilder builder, int ct)
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    DrawSqaure(builder, i * 10, j * 10, ref ct);
                }
            }
            return ct;
        }

        private void DrawSqaure(RenderTreeBuilder builder, int dx, int dy, ref int ct)
        {
            builder.OpenElement(ct++, "rect");
            builder.AddAttribute(ct++, "x", dx);
            builder.AddAttribute(ct++, "y", dy);
            builder.AddAttribute(ct++, "width", 100);
            builder.AddAttribute(ct++, "height", 100);
            builder.CloseElement();
        }
    }
}
