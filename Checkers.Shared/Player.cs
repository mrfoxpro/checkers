﻿namespace Checkers.Shared
{
    public class Player
    {
        public string NickName { get; set; }
        public string ID { get; set; }

        public PlayerStatus Status;

        public Lobby Lobby { get; set; }
    }
}