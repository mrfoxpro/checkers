﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Checkers.Shared
{
    public class Lobby
    {
        public string Name { get; set; }
        public string ID { get; set; }
        public Player Player1 { get; set; }
        public Player Player2 { get; set; }
    }
}
