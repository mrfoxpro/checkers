﻿namespace Checkers.Shared
{
    public enum PlayerStatus
    {
        Lobby,
        Waiting,
        InGame
    }
    public enum LobbyStatus
    {
        Waiting,
        InGame
    }
    public enum CheckersColor
    {
        Black,
        White
    }
}